FAQ
===

About UTU
---------

What is UTU?
~~~~~~~~~~~~

UTU is building the trust infrastructure of the internet to help businesses and consumers engage and transact in an easier, safer, and more trustworthy way.

Our AI-based API products collect and analyze data to create trust signals and personalized recommendations that help consumers and businesses make the best decisions for their situation. And the UTU blockchain protocol rewards users for trustworthy actions and compensates them for sharing their data while protecting their privacy.

UTU changes the economics of trust, ensures trust can’t be bought or manipulated, and leverages data to help people make better decisions.

UTU is the Swahili word for “humanity” and reflects our roots in Nairobi, Kenya.


What problem is UTU solving?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In any online marketplace, establishing trust between parties who would like to engage in business with each other is a prerequisite for that business to actually happen. But the current trust and reputation mechanisms such as anonymous star ratings and reviews are often flawed and easily manipulated. Consider, for instance, the fake restaurant that reached #1 on TripAdvisor London and widespread review fraud on Amazon.


How are we solving this?
~~~~~~~~~~~~~~~~~~~~~~~~

We are building the trust infrastructure of the internet with our AI-based APIs. The UTU Trust API replaces typical rating and review tools and uses AI to adapt to users’ unique preferences and context. This allows businesses to provide personalized recommendations to increase conversions and sales. Additionally, the UTU protocol uses blockchain to ensure reviews and ratings can’t be manipulated. The protocol compensates users who provide accurate ratings and allows full control of data privacy.


What are the advantages of UTU over other review systems?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Current online trust systems feature anonymous ratings and reviews that are often manipulated. Trustpilot, Google and Amazon are constantly working to remove millions of fake reviews. Furthermore, current trust mechanisms don’t reflect how we trust in the real world. Most people trust friends and family recommendations over strangers online. UTU’s Trust API replaces typical rating and review tools, and uses social connections and artificial intelligence to provide users with the most trusted recommendations from people they know.


How UTU helps businesses and customers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By bridging the gap between how we trust in real life and how we are asked to trust online, UTU eliminates the problems that fake reviews present — poor purchase decisions for consumers, and harmed reputations and lost sales for online businesses.

By offering personalized recommendations, online businesses enjoy the following benefits:

- Increased conversion rates.
- Decreased customer acquisition cost.
- Improved retention, satisfaction, lifetime value, and viral acquisition.
- Increases revenue from improved upsell and cross-sell capabilities.
- Increases brand value around transparency, privacy, and respect for user data.

Customers are more informed about their purchases and can make decisions in a more trustworthy environment, improving their experience and general well-being.


What is the UTU Roadmap?
~~~~~~~~~~~~~~~~~~~~~~~~

You can find `our public roadmap here <https://ututrust.notion.site/d0305dd836b6473abba563392e6d37e1?v=39ec586f68c0493ea804a50de8001b3b&pvs=4>`_.


How can I contact UTU?
~~~~~~~~~~~~~~~~~~~~~~~

Reach out to us on `trust@utu.io <mailto:trust@utu.io>`_ or on any of our social media channels.

- `Twitter <https://twitter.com/ututrust>`_
- `Telegram <https://t.me/UTUtrust>`_
- `Reddit <http://reddit.com/r/ututrust>`_
- `YouTube <https://youtube.com/channel/UCwx0iSVJWoWwaKfCajigbBQ/>`_
- `LinkedIn <https://www.linkedin.com/company/utu-technologies/about/>`_
- `Instagram <http://instagram.com/ututrust>`_
- `Facebook <http://facebook.com/ututrust>`_

You can also check out our `website <https://utu.io/about-us/>`_ for more information about the team and vision.


Products
---------

Trust API
~~~~~~~~~

The UTU Trust API replaces typical rating and review tools and uses AI to adapt to users’ unique preferences and context. This allows businesses to provide personalized recommendations to increase conversions and sales.

Browser Extension
~~~~~~~~~~~~~~~~~

The UTU Browser Extension allows users to view  feedback from their network about any website they're visiting, and to leave feedback and make staked endorsements to earn UTU tokens for their contributions. Learn more about it `here <https://utu.io/blog/utu-trust-browser-extension-now-live-on-chrome-browsers/>`_, or `install it directly in Chrome <https://chrome.google.com/webstore/detail/utu-browser-extension-sta/hlkndlgadndnloojfnjccoglobgjlocd>`_ (coming to Firefox soon, or `import it from your Chrome installation <https://www.ghacks.net/2023/08/23/firefox-users-may-import-chrome-extensions-now/>`_).

Creditworthiness API
~~~~~~~~~~~~~~~~~~~~

The UTU Creditworthiness API that is now available for lenders in traditional and decentralized finance platforms. This API leverages machine learning to continuously improve the accuracy and precision of credit assessments, identifying customers that are trustworthy and those that have a higher risk of default. Learn more about the `Creditworthiness API here <https://utu.io/blog/the-utu-creditworthiness-api-mvp-is-live/>`_.


M-PESA Parser API
~~~~~~~~~~~~~~~~~

The M-PESA Parser API allows clients to easily extract data, such as name, phone number, and account payment activity, from their customers’ M-PESA statements. Clients will be able to view this information on a dashboard and incorporate it into our Creditworthiness API or the credit scoring model of their choice.


DeFi Portal
~~~~~~~~~~~

The DeFi portal MVP helps you identify DeFi protocols that are trustworthy to interact with. After you enter your Ethereum wallet address into the portal, our trust engine scans the Ethereum network for the last 100 transactions that you’ve executed with your address. Then it will identify addresses that you’ve interacted with in the past, scan the last 100 transactions for these addresses, and display the protocols that you have in common.
Learn more about the DeFi portal `here <https://utu.io/blog/announcing-utus-defi-portal-mvp/>`_.


The UTU Protocol
-----------------

Where is the UTU White Paper?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can find the `UTU White Paper here <https://docs.google.com/document/u/1/d/e/2PACX-1vQvaQZv9iFPRAkWZsSJu_tT4z1Q8uXalRKVHEv5cbS5DxcbqPB4UUhALj2dnWcsa-jEFeyxqCqJKh0m/pub>`_.


How does the UTU Protocol work?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Fake and inaccurate reviews can lead to poor purchase decisions. The UTU protocol leverages blockchain to ensure that reviews can’t be manipulated.

The UTU protocol uses blockchain to reward trustworthy actors and ensure reviews and ratings can’t be manipulated. You can earn from the reviews you create and the data you share with apps that you use. With UTU, you have full control over the data you share.


Tokenomics (UTT and UTU)
~~~~~~~~~~~~~~~~~~~~~~~~~

You earn UTU Trust Tokens (UTT) for providing your reviews and data to the protocol. UTT is non-transferable but can be converted into UTU Coin, but not the other way around. This prevents people from “buying” trust.

UTU Coin (UTU) is the currency of the UTU protocol and is used as a form of payment within the UTU app ecosystem. It is required to use our trusted recommendation service and has a max supply of 1 billion.


UTU Coin
~~~~~~~~

How can I buy UTU Coin?
^^^^^^^^^^^^^^^^^^^^^^^^

What exchanges is UTU Coin listed on?
You can buy UTU Coin ($UTU) on:

- `Uniswap <https://info.uniswap.org/#/pools/0x098e0b9de8ed064006d9dda99b4e3d1604801cea>`_
- `PancakeSwap <https://exchange.pancakeswap.finance/#/add/BNB/0xed4Bb33F20F32E989AF975196E86019773A7CFf0>`_
- `Bibox <https://www.bibox.com/en>`_
- `HotBit <https://www.hotbit.io/>`_

Visit `CoinGecko <https://www.coingecko.com/en/coins/utu-coin>`_ or `CoinMarketCap <https://coinmarketcap.com/currencies/utu-protocol/>`_ to keep track of the price and access more information.


Are you listing on more exchanges?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Yes, we’re continually working to list UTU coin on more centralized and decentralized exchanges. Listing on exchanges costs money, so we have to work through it carefully.


Cross-chain interoperability
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

UTU Coin is available on both Ethereum and Binance Smart Chain and you can also port your UTU in both directions.

You can use Meter Passport, Meter’s bridge application, to port your UTU on Ethereum to BSC 1-to-1, and vice versa. You’ll then be able to trade UTU on BSC or add liquidity to our UTU-BNB pool on PancakeSwap. `Learn more here <https://utu.io/blog/how-to-use-meter-passport-to-bridge-utu-coin-on-ethereum-to-bsc/>`_.

We have also partnered with many other protocols such as Elrond, Polygon, Oasis, Harmony, and others to deploy UTU Coin and our trust infrastructure on their blockchains.


Tokenomics / Token Release Schedule
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Learn more about the UTU Coin release schedule `here <https://utu.io/blog/updates-to-the-tokenomics-and-release-schedule-of-utu-coin/>`_.

