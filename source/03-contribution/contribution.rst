.. _contribution:

How to Contribute
=================

Many of UTU's components are open source and open to community contributions. We're also going to award bounties for certain especially requested contributions. This chapter explains how to contribute to UTU's open source projects, and how to claim bounties.

Components Open For Contributions
---------------------------------

For an architecture overview, please see the :ref:`Architecture section in the Integration chapter <architecture>`.

Generally, our open source repositories which are open for contributions are found in our `UTU Technologies Gitlab group <https://gitlab.com/ututrust>`_.

Among those, the repositories which might be especially relevant for contributions are:

- `UTU App <https://gitlab.com/ututrust/utu-protocol/utu-app>`_
- `Social Media Connector <https://gitlab.com/ututrust/api-sdk/utu-trust-social-media-connector>`_
- `UTU Web SDK <https://gitlab.com/ututrust/api-sdk/utu-trust-sdk>`_
- `UTU Browser Extension <https://gitlab.com/ututrust/api-sdk/utu-browser-extension>`_
- `Blockchain transaction scanner <https://gitlab.com/ututrust/utu-protocol/defi-portal-scanner>`_
- `UTU Trust Token Subgraph <https://gitlab.com/ututrust/utu-protocol/utt-subgraph>`_
- `UTU Trust Token <https://gitlab.com/ututrust/utu-protocol/utu-trust-token>`_
- `Documentation (the source of these pages here) <https://gitlab.com/ututrust/documentation>`_

Each of these repositories has a README file which explains the purpose of the repository and its architecture.

Contribution Workflow
----------------------

To contribute to UTU, please follow the simple steps below. The workflow is basically the same for any kind of contribution, be it for claiming a bounty, a hackathon prize, or just for fun.

However, in case of bounties/prizes, the final review step might be more rigorous, and there might be competition from other developers or teams, where not all contributions might get rewarded. Please check the bounty / prize description for details.

In any case, you're welcome to contact the UTU dev team or any other contributors on any available channels such as Discord, Telegram, Gitlab, email, or others (though specific devs might have preferred channels on which they react quicker).

Step 1.: Preparation
''''''''''''''''''''

To setup your git work branch:

1. Create (for a new component) or fork (for an extension or fix of an existing component) the bounty repository on your Gitlab or Github account. All our repositories are hosted primarily on Gitlab, but are mirrored on Github for convenience of developers who prefer Github (if a repository's Github mirror is missing or outdated, please let us know).
2. Clone the repository on your local machine.
3. Create and push a new branch off main branch to work on. Please name it according to the type of your contribution, to keep things better organised:

    - For a bounty, name it ``bounty/<number>-<short-description>``, e.g. ``bounty/1-connect-discord``.
    - For a hackathon prize, name it ``<hackathon-name>/<short-description>``, e.g. ``ethsafari-bootcathon-2023/connect-discord``.
    - For a general contribution, name it ``feature/<short-description>``, e.g. ``feature/connect-discord``.
    - For a bug fix, name it ``bug/<short-description>``, e.g. ``bug/fix-discord``.

4. On Gitlab, create a *draft* Merge/Pull Request:

    - Subject: "\<branch name> Review"
    - Description: write a summary of your approach. This will make it easy for the reviewer to comment through your code at one glance as opposed to moving in between your commits.


Step 2.: Implementation and Testing
'''''''''''''''''''''''''''''''''''

1. Implement your work following the :ref:`code quality guidelines below <code-quality-guidelines>`. Preferably commit your work in small, logical chunks.
2. Double check that you are meeting all the requirements. Edge cases, Catching errors properly, UI considerations, etc. It might also be helpful to get at least 1 other person (from the UTU dev team, other contributors, or other devs) to look at and discuss your work during this stage.


Step 3.: Submission
'''''''''''''''''''

When you’re sure that your work is solid,

1. Go ahead and record a screencast of your functionality, not longer than 5 minutes [#]_. You might use audio or text subtitles for explanations. Keep them straight to the point. In case of audio, ensure you are audible and clear. Adding extra swagu to the video is a plus.
2. Add a link to the screencast in the merge/pull request description, if possible. If that's not possible for some reason, you can also send the video file directly to the reviewers:
3. Mark the merge/pull request as ready and assign repository maintainers as reviewers, or, in case of a new repository, assign the UTU dev team.
4. Reviewers _might_ request a live session, during which you'd walk your reviewer through your code. Use it to point out interesting techniques used, tools you leveraged, etc.
5. If the reviewers

    1. are satisfied, they will merge your merge/pull request. And if there was a bounty or prize on the contribution, and it was awarded, you will now receive your reward. Congratulations! 🎉 🎉 🎉
    2. are not yet satisfied, but have change requests, please follow their instructions how to proceed. Typically, this would be adding some minor changes to the merge/pull request, and then letting the reviewers know and continue from step 3.
    3. are principally not satisfied, they will close the merge/pull request. In this case, you can still continue to work on your contribution, and open a new merge/pull request when you're ready. If the functionality changed much, you should also record a new screencast and go back to step 1. If you think the reviewers were wrong, you can also discuss this with them, or with other devs, on any of the available channels.

.. [#] For command line applications, you can use e.g. asciinema (`a sample <https://asciinema.org/a/ecf1yzu8gvhiwwtg5ry4fuoil>`_). For other applications, you could use Quicktime or any other screen recorder.

.. _code-quality-guidelines:

Code Quality Guidelines
-----------------------

Architecture, Code Design and Style
'''''''''''''''''''''''''''''''''''

- All code shall be well documented. Abstractions such as functions, classes, etc., especially non-private or non-trivial ones, shall have docs in the format of the language's standard documentation system (e.g. `JSDOC <https://jsdoc.app>`_, `Godoc <https://go.dev/blog/godoc>`_, etc.).
- In-code comments shall be used where necessary, but only for code which is not self-explanatory.
    - Good example::

        // Optionally match the pattern, because if we matched non-optionally, the whole
        // query would return nothing if no targets match any of the patterns.
        queryBuilder.optionalMatch(patterns);

      While it might be quite clear what `queryBuilder.optionalMatch()` does, the reason why we don't use the simpler `queryBuilder.match()` is not. So a comment is appropriate here.
    - Bad example::

        // Do this only if x is less than 1
        if(x < 1) { ... }

      This is a bad example because it's obvious what the condition does, and the comment provides no simpler explanation or otherwise useful information. A better comment would explain _why_ we want to execute the block only if `x` is less than 1, if this is not obvious from the context.

- Architecture, code design and style shall be clean and according to reasonably up-to-date industry practices and standards. Some maybe helpful references:

  - Many references in `Awesome Software Architecture <https://github.com/mehdihadeli/awesome-software-architecture>`_
  - `10 Common Broken Rules of Clean Code <https://www.kamilgrzybek.com/blog/posts/10-common-broken-clean-code-rules>`_
  - Especially Solidity or other smart contract coding, special considerations might need to be taken. E.g. naively applying otherwise well-accepted principles such as `DRY <https://en.wikipedia.org/wiki/Don%27t_repeat_yourself>`_ or the `Extract Function refactoring <https://refactoring.com/catalog/extractFunction.html>`_ `might` [#]_ lead to inefficient and more costly code, quite literally in terms of gas. `Awesome Solidity <https://github.com/bkrem/awesome-solidity>`_ has many good references to properly learn Solidity coding.
- But code which extends existing repositories shall also adhere to architectures and code styles (e.g. OOP vs. FP) of the existing codebase. If you think (part of) a component's architecture or code style should be changed/refactored for any reason, please discuss this with the repository's maintainers first.
- Code style definition files for popular IDEs and code editors shall be provided. If none such exists yet, and you're extending an existing repository, use its existing style.
- When in doubt, please don't hesitate to ask and discuss with maintainers on any of the provided channels. Though for better transparency also towards other devs, it might be preferable to discuss in comments on Gitlab or Github issues, or in draft merge/pull requests.
- You can also schedule time during UTU community hours to talk about your progress and how you can improve on what you’re currently doing.

.. [#] Or it might not, because `recent Solidity compiler versions can automatically automate some of such cases <https://docs.soliditylang.org/en/latest/internals/optimizer.html#simple-inlining>`_. But it's important to be aware of such considerations, and know when something should be optimised for readability and maintainability, and when for efficiency/gas price.

Tests
'''''''

Test Coverage shall be near complete and include all added main functionality, and of course your added tests shall succeed when a merge/pull request is marked as ready (i.e. is not just a draft anymore). Test coverage and execution are (to be) checked automatically using Gitlab's CI/CD. We generally distinguish between unit tests and integration tests:

  - Unit tests run without any external dependencies. They are generally fast and shall be used wherever possible.
  - Integration tests might setup databases or other external systems for testing. End-to-end tests are usually a specific kind of integration tests.


Specific Types of Contributions
-------------------------------

.. _creating_connector:
Creating a Connector
''''''''''''''''''''

A Connector is used to retrieve the connections of a logged in user from a Social Media Platform.

For the purpose of hackathons and bounty programmes a new Connector is pushed to the following repositories:

Front end: `https://gitlab.com/ututrust/api-sdk/connectors-frontend-community <https://gitlab.com/ututrust/api-sdk/connectors-frontend-community>`__

Back end:  `https://gitlab.com/ututrust/api-sdk/connectors-backend-community <https://gitlab.com/ututrust/api-sdk/connectors-backend-community>`__

To get started please read:

`https://gitlab.com/ututrust/api-sdk/connectors-frontend-community/-/blob/main/README.md <https://gitlab.com/ututrust/api-sdk/connectors-frontend-community/-/blob/main/README.md>`__

On successful completion and award of your bounty for the connector, UTU staff will refactor your code into 2 other repositories:

(i) `https://gitlab.com/ututrust/utu-app <https://gitlab.com/ututrust/utu-app>`__

(ii) `https://gitlab.com/ututrust/utu-trust-social-media-connector <https://gitlab.com/ututrust/utu-trust-social-media-connector>`__

