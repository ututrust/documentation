.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UTU
==============

*UTU’s vision is to make the internet a safer, more trusted place to gather, share, work, and trade.*

`UTU <https://utu.io>`_ is building the trust infrastructure of the internet to help businesses and consumers engage and transact in an easier, safer, and more trustworthy way.

Our AI-based API products collect and analyze data to create trust signals and personalized recommendations that help consumers and businesses make the best decisions for their situation. And the UTU blockchain protocol rewards users for trustworthy actions and compensates them for sharing their data while protecting their privacy.

UTU changes the economics of trust, ensures trust can’t be bought or manipulated, and leverages data to help people make better decisions.

Continue to learn how to integrate UTU into your (d-)app, how to contribute to UTU, or check out the FAQ to find an answer to your question.

Reach out if you need any further help!


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   01-introduction/introduction.rst
   02-integration/integration.rst
   03-contribution/contribution.rst
   04-faq/faq.rst

Indices and Tables
==================

* :ref:`genindex`
* :ref:`search`
