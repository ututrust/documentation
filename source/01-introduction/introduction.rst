Introduction to the UTU Trust Engine
====================================

What it is
----------

The UTU Trust Engine is a revolutionary solution designed to bring a
real model of human trust to online platforms, marketplaces, and
decentralized applications. By incorporating trust models into digital
interactions, the UTU Trust Engine aims to enhance the trustworthiness
of transactions and improve the overall user experience when engaging
with various services and products online.

Example Use Case
----------------

As an example, let's consider a decentralised exchange (DEX). Many DEXs allow users to load custom token lists or specific token addresses that they got somewhere, like someone on Twitter or Telegram. However, users have no way of knowing if these tokens are legitimate or if they are being scammed. The UTU Trust Engine can help by providing relevant feedback to users about the tokens they are trading.
Specifically, the UTU Trust Engine can provide feedback such as:
"Your contacts Alice and Bob trust this specific ERC-20 token." UTU can also provide more nuanced information, such as: “Your fellow traders Alice and Bob trust this ERC-20 token, and they have frequently traded it on this exchange.”

Documentation Outline
---------------------

The rest of this document is structured as follows:

- In the rest of this chapter, we introduce the main ideas, architecture, and concepts of the UTU Trust Engine.
- In chapter :ref:`How to Integrate <integration>`, we describe how to integrate UTU into your app.
- In chapter :ref:`How to Contribute <contribution>`, we describe how to contribute extensions and bug fixes to the UTU Trust Engine, often for rewards.

Serving Relevant Feedback
-------------------------

To serve relevant feedback, UTU's Trust Engine considers social connections between users, the tokens they trade, and pertinent context data. In the above example, the trust analyser determines that:

1. Alice and Bob are connected to the currently signed-in user.
2. They have traded the specific ERC-20 token in the past on the same exchange.

For this to function, UTU's API and SDK provide appropriate endpoints and UI components to gather this data. Data is stored in UTU's shared global database, adhering to strict access controls. Users must permit their data to be used for feedback purposes and have control over how it is utilized.

.. _architecture:

Architecture
------------

The UTU Trust Engine is composed of these main components:

1. **Backend API**: A centralised backend that offers endpoints for
   creating and managing entities, relationships between entities, and
   feedback. The API also provides endpoints for users to access
   feedback summaries and details about entities from other users who
   are related to them, either directly or indirectly.

2. **Web SDK**: A comprehensive software development kit that provides
   user-friendly UI elements for viewing and creating staked
   endorsements and other feedback.

3. **UTU Trust Token (UTT)**: A blockchain-based smart contract that
   enables users to endorse entities and earn rewards through a
   transparent and independently verifiable process. The functionality of the smart contract and its tokenomics is described in detail in the `UTU Whitepaper <https://docs.google.com/document/d/e/2PACX-1vQvaQZv9iFPRAkWZsSJu_tT4z1Q8uXalRKVHEv5cbS5DxcbqPB4UUhALj2dnWcsa-jEFeyxqCqJKh0m/pub>`_.

4. **UTU App and social media connector**: This enables users to connect
   with social networks in order to show more relevant feedback to them
   from their own network.

5. **Blockchain transaction scanner**: Connected user addresses are
   scanned for transactions with other users’ addresses as well as smart
   contracts, to further extend the user’s network and identify which
   smart contracts have been used by other users in it.

Here is a diagram of the main components, including the
user’s browser running the UTU Browser Extension or decentralized apps
that integrate UTU:

.. Note: when the diagram was changed, find the new file name in _build/html/_plantuml/... and update the link here:
.. uml:: UTU Protocol Deployment.puml
   :caption: `UTU Trust Engine Main Components <../_plantuml/86/862f0e4f74740ddcbdbb48f475007b5014bb43a1.svg>`_
   :align: center

As you can see, the UTU Trust Token (UTT) smart contract ensures
transparency and verifiability for entity endorsements.

.. _main-concepts:

Main Concepts
-------------

The UTU Trust Engine uses the following core concepts:

- **Entities** are the main objects of the UTU Trust Engine. They can be users, products, services, or any other object that can provide feedback and/or be
  reviewed. Each entity has

    - a type, which can be freely determined by using applications,
    - a UUID (universally unique identifier) in the format of an Ethereum address [#]_,
    - a name and
    - optionally an image link.

- **Relationships** are connections between a *source* and a *target* entity, and they can be uni- or bi-directional. They further have one of a few different supported types, the most important ones being:

    - `social`: the two entities represent two persons who are socially related;
    - `endorsement`: the source endorsed the target (on-chain);
    - `feedback`: the source provided feedback about the target;
    - `interaction`: the source interacted with the target, for example by sending them some blockchain tokens.

- **Feedback** items are the contents of provided feedback about a target entity, including endorsements and their staked UTU Trust Tokens (UTT).
- **Feedback summary** is a summarised view (currently a short text) of a set of feedback items about a target entity.
- **Ranking** is an ordering of a given set of entities relative to each other, as interpreted by the UTU Trust Engine based on feedback from the network of the user who requested the ranking. It is important to note that UTU does _not_ assign a global "trust score" or "trust value" to any entity, but rather aims to provide relevant feedback, both in summary and in detail, from the requesting user's own network. Therefore, a global (objective) ranking of entities does not exist.

.. [#] The UTU Trust Engine actually supports all kinds of UUIDs and other named IDs, as long as they identify the entity uniquely or within their type and their used database, respectively. However, when using UTU's main global database, as is currently required for all integrating apps, the UUIDs must be in the format of an Ethereum address. This is because endorsements about entities are stored in the UTT smart contract, which uses Ethereum addresses as identifiers.
   For entities whose "natural" UUID is not in the format of an Ethereum address, the latter can be easily obtained. E.g. when using Javascript with `ethers <https://ethers.io>`_ by `ethers.utils.id("utu.io").slice(0, 40 + 2);`


